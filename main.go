package main

import (
	"github.com/joho/godotenv"
	"gitlab.com/barcos.co/gocore"
	ownrepositories "gitlab.com/barcos.co/gocoreexample/ownrepositories"
	ownservices "gitlab.com/barcos.co/gocoreexample/ownservices"
)

func main() {

	godotenv.Load()
	gocore.LoadSystemConfigs()

	server := gocore.NewServer().SetApiRoot("/api/v1")

	authRepo := ownrepositories.NewAuthRepository()
	authService := gocore.NewAuthService()
	authMessagingService := ownservices.NewMessagingClient()

	authController := gocore.NewAuthController(authRepo, authService, authMessagingService)
	authController.SetApiRoot(server.GetApiRoot())

	// /api/v1/oauth2/token?grant_type=password
	// Basic Auth
	// /api/v1/oauth2/token?grant_type=refresh_token&refresh_token=some-token.jwt
	authController.SetTokenPath("/oauth2/token")
	authController.SetRegistrationPath("/oauth2/register")
	authController.SetVerifyAccountPath("/oauth2/verify_account")
	authController.SetResetPasswordPath("/oauth2/reset_password")

	login := gocore.NewPostRouter(authController.GetTokenPath(), authController.Token)
	register := gocore.NewPostRouter(authController.GetRegistrationPath(), authController.Register)
	verifyAccount := gocore.NewGetRouter(authController.GetVerifyAccountPath(), authController.VerifyAccount)
	resetPassword := gocore.NewPostRouter(authController.GetResetPasswordPath(), authController.ResetPassword)

	server.
		AddRouter(login.SetAPIKeyRequired()).
		AddRouter(register.SetAPIKeyRequired()).
		AddRouter(verifyAccount.SetAPIKeyRequired()).
		AddRouter(resetPassword.SetAPIKeyRequired()).
		SetSystemConfig(*gocore.GetSystemConfigs()).
		Start()
}
