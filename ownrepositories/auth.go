package ownrepositories

import (
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/models"
	"gitlab.com/barcos.co/gocore/models/base"
	"gitlab.com/barcos.co/gomongodb"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type AuthRepository struct {
	mongoClient    *gomongodb.MongoDBClient
	authController interfaces.IAuthController
}

func NewAuthRepository() *AuthRepository {
	gomongodb.LoadMongoSettings()
	return &AuthRepository{mongoClient: gomongodb.NewMongoClient()}
}

func (repository *AuthRepository) GetAuthController() interfaces.IAuthController {
	return repository.authController
}

func (repository *AuthRepository) SetAuthController(authController interfaces.IAuthController) {
	repository.authController = authController
}

func (repository *AuthRepository) FindUser(email string) (*models.User, errors.Error) {
	err := errors.NewError(nil)
	var user models.User

	if repository.mongoClient != nil {
		err = repository.mongoClient.FindOneDocumentByQuery(primitive.M{"email.email": email}, &user, UsersCollectionName)
		return &user, err
	}

	// This shouldn't occur
	err.SetInternalServerError()
	return &user, err
}

func (repository *AuthRepository) UpdateUser(email string, data interfaces.UpdateData) (*models.User, errors.Error) {

	data = primitive.M{"$set": data}
	someErr := repository.mongoClient.UpdateOneDocumentByQuery(primitive.M{"email.email": email}, data, UsersCollectionName)
	if !someErr.IsNilError() {
		return nil, someErr
	}

	return nil, someErr.SetNilError()
}

func (repository *AuthRepository) VerifyUser(email string) (bool, errors.Error) {
	err := errors.NewError(nil)
	return false, err
}

func (repository *AuthRepository) AddUser(user *models.User) (string, errors.Error) {
	var someErr errors.Error
	result, someErr := repository.mongoClient.InsertOneDocument(user, UsersCollectionName)
	if !someErr.IsNilError() {
		return "", someErr
	}
	return result, someErr.SetNilError()
}

func (repository *AuthRepository) DeleteUser(email string) (bool, errors.Error) {
	err := errors.NewError(nil)
	return false, err
}

func (repository *AuthRepository) Connect() (bool, errors.Error) {
	err := errors.NewError(nil)
	return false, err
}

func (repository *AuthRepository) FindDocument(Id string) (*base.Document, errors.Error) {
	err := errors.NewError(nil)
	return &base.Document{}, err
}

func (repository *AuthRepository) UpdateDocument(Id string, data interfaces.UpdateData) (*models.User, errors.Error) {
	err := errors.NewError(nil)
	return &models.User{}, err
}

func (repository *AuthRepository) AddDocument(document base.Document) (string, errors.Error) {
	err := errors.NewError(nil)
	return "AddDocument", err
}

func (repository *AuthRepository) DeleteDocument(Id string) (bool, errors.Error) {
	err := errors.NewError(nil)
	return false, err
}
