# OAuth2 Example Server

Auth Server written in Golang for example purposes
This project use a `MongoDB` client named `AuthRepository` and a `Mailjet` client named `MessagingClient` as SMTP server. Both clients written by me. You could create your own clients for other services. Make sure to follow the interfaces for each client

0. Create a valid `.env` file. Follow the `.env.example`
1. For running in develpment mode `go run main.go` 
2. For build and run the executable `go build main.go && /main`. The executable make use of one `.env` file
3. Everytime the `gocore` or other deps need to be updated `go mod tidy`
4. If you are planning to run on production mode then make sure the `.env` file has production configs

## License
MIT