package ownservices

import (
	"gitlab.com/barcos.co/gocore/errors"
	"gitlab.com/barcos.co/gocore/interfaces"
	"gitlab.com/barcos.co/gocore/models/request"
	mailjet "gitlab.com/barcos.co/gomailjet"
)

type MessagingClient struct {
	Name           string
	mailjetClient  mailjet.MailjetClient
	authController interfaces.IAuthController
}

func NewMessagingClient() *MessagingClient {
	mailjet.LoadMailjetConfigs()
	return &MessagingClient{Name: "MailjetClient", mailjetClient: mailjet.NewMailjetClient()}
}

func (client *MessagingClient) GetAuthController() interfaces.IAuthController {
	return client.authController
}

func (client *MessagingClient) SetAuthController(authController interfaces.IAuthController) {
	client.authController = authController
}

func (client *MessagingClient) SendEmail(email string) (bool, errors.Error) {
	return client.mailjetClient.SendEmail(email)
}

func (client *MessagingClient) SendSMS(number string) (bool, errors.Error) {
	return client.mailjetClient.SendSMS(number)
}

func (client *MessagingClient) SendIOSPushNotification(deviceId string) (bool, errors.Error) {
	return client.mailjetClient.SendIOSPushNotification(deviceId)
}

func (client *MessagingClient) SendVerifyEmail(data request.AuthRegisterMessagingPayload) (bool, errors.Error) {
	return client.mailjetClient.SendVerifyEmail(data)
}

func (client *MessagingClient) SendResetPasswordEmail(data request.AuthResetPasswordEmailMessagingPayload) (bool, errors.Error) {
	return client.mailjetClient.SendResetPasswordEmail(data)
}
